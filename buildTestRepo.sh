#!/bin/sh
rm -rf testRepo
mkdir testRepo
cd testRepo
mkdir packages
cd packages 
../../cspm.py build ../../testPackages/test/package.cspm
../../cspm.py build ../../testPackages/test2/package.cspm
cd ..
echo "<packages>" > cspmrepo.xml
../cspm.py xml ../testPackages/test/package.cspm >> cspmrepo.xml
../cspm.py xml ../testPackages/test2/package.cspm >> cspmrepo.xml
echo "</packages>" >> cspmrepo.xml
